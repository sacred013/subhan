package MyPack;
import java.util.Arrays;
public class HW14 {
    public static void main(String[] args) {
        int [] arr = {4, -5, 0, 6, 8};
        int max = arr[0], min = arr[0], maxInd = 0, minInd = 0;
        for (int i = 0; i< arr.length; i++){
            if (max<arr[i]){
                max = arr[i];
                maxInd = i;
            }
            else if (min>arr[i]) {
                min = arr[i];
                minInd = i;
            }
        }
        arr[maxInd] = min;
        arr[minInd] = max;
        System.out.println(Arrays.toString(arr));
    }
}