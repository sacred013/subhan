package MyPack;
import java.util.Scanner;
public class HW7 {
    public static void main(String[] args) {
        Scanner in = new Scanner(System.in);
        System.out.print("Введите число: ");
        int number = in.nextInt();
        int result = 1;
        for (int i = 1; i <= number; i++)
            result *= i;
        System.out.println("Факториал числа: " + number + " равен " + result);
    }
}