package MyPack.mypackage;

import java.util.Scanner;

public class Dog {

    Scanner in = new Scanner(System.in);
    private String name;
    private String color;
    private int age;

    public Dog (){
        System.out.print("Введите имя собаки: ");
        this.name = in.next();
        System.out.print("Введите цвет собаки: ");
        this.color = in.next();
        System.out.print("Введите возраст собаки: ");
        this.age = in.nextInt();

    }
    public void Action() {

        int change = 0;
        while (change != 4) {
        System.out.println("Выберите действие: " +
                "\n 1. Поменять имя собаки" +
                "\n 2. Поменять цвет собаки" +
                "\n 3. Поменять возраст собаки" +
                "\n 4. Выбрать место действия \n");
        switch (change = in.nextInt()) {
            case 1:
                System.out.println("Введите новое имя собаки: ");
                this.name = in.next();
                break;
            case 2:
                System.out.println("Введите новый цвет собаки: ");
                this.color = in.next();
                break;
            case 3:
                System.out.println("Введите новый возраст собаки: ");
                this.age = in.nextInt();
                break;
            case 4:
                place();
                break;
            }
        }
    }

    public void place() {

        System.out.println("Выберите место: " +
                "\n 1. Home" +
                "\n 2. Park" +
                "\n 3. Dog Trainer \n");
        switch (in.nextInt()){
            case 1: actionHome(); break;
            case 2: actionPark(); break;
            case 3: actionDogTrainer(); break;
        }
    }

    public void actionHome() {

        System.out.println("Выберите действие: " +
                "\n 1. Поспать" +
                "\n 2. Посидеть" +
                "\n 3. Полежать" +
                "\n 4. Поиграть" +
                "\n 5. Поесть \n");
        switch (in.nextInt()){
            case 1:
                System.out.println(name + ": Спим дрыхнем дома\n"); break;
            case 2:
                System.out.println(name + ": Сидим чилим дома\n"); break;
            case 3: System.out.println(name + ": Я лежу дома, Хозяин\n"); break;
            case 4: System.out.println(name + ": Играю дома с Хозяином\n"); break;
            case 5: System.out.println(name + ": Доби дома, сыт и доволен\n"); break;
        }
    }
    public void actionPark() {
        System.out.println("Выберите действие: " +
                "\n 1. Гулять" +
                "\n 2. Играть" +
                "\n 3. Бежать" +
                "\n 4. Лаять" +
                "\n 5. Лежать\n");
        switch (in.nextInt()){
            case 1:
                System.out.println(name + ": Гуляем в парке с Хозяином\n"); break;
            case 2:
                System.out.println(name + ": Играю с Хозяином в парке\n"); break;
            case 3: System.out.println(name + ": Я бегу по парку, я доволен\n"); break;
            case 4: System.out.println(name + ": В парке Гав Гав\n"); break;
            case 5: System.out.println(name + ": Я лежу в парке на траве, Хозяин\n"); break;
        }
    }
    public void actionDogTrainer() {

        System.out.println("Выберите действие: " +
                "\n 1. Сидеть!" +
                "\n 2. Лежать!" +
                "\n 3. Рядом!" +
                "\n 4. Голос!" +
                "\n 5. Ко мне!" +
                "\n 6. Вольно!\n");
        switch (in.nextInt()){
            case 1:
                System.out.println(name + ": Я сижу, Учитель!\n"); break;
            case 2:
                System.out.println(name + ": Я лежу, Учитель!\n"); break;
            case 3: System.out.println(name + ": Я сижу справа от Учителя!\n"); break;
            case 4: System.out.println(name + ": Гав Гав отвечаю Учителю\n"); break;
            case 5: System.out.println(name + ": Я бегу к Учителю\n"); break;
            case 6: System.out.println(name + ": Учитель отпустил, Доби свободен!☻\n"); break;
        }
    }
}