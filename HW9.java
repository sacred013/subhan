package MyPack;
import java.util.Scanner;
public class HW9 {
    public static void main(String[] args) {
        Scanner in = new Scanner(System.in);
        System.out.print("Введите число возводимое в степень: ");
        int number = in.nextInt();
        int result = number;
        System.out.print("Укажите степень числа: ");
        int numbersDegree = in.nextInt();
        for (int i = 2; i<=numbersDegree; i++) {
            result *= number;
        }
        if (numbersDegree == 0 && number != 0) {
            result = 1;
            System.out.println("Ответ: " + result);
        }
        else if (number == 0 && numbersDegree == 0) {
            System.out.println("Нуль в нулевой степени не определен, такое выражение не имеет смысла.");
        }
        else System.out.println("Ответ: " + result);
    }
}