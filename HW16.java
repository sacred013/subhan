package MyPack;
import java.util.Arrays;
public class HW16 {
    public static void main(String[] args) {
        int [] arr = new int [20];
        int min = 0;
        int max = 1001;
        boolean sort = true;
        for (int i = 0; i < arr.length; i++){
            arr [i] = min + (int) (Math.random() * max);
        }
        while (sort){
            sort = false;
            for (int i = 0; i < arr.length-1; i++){
                if (arr[i] < arr[i+1]){
                    int template = arr[i];
                    arr[i] = arr[i+1];
                    arr[i+1] = template;
                    sort = true;
                }
            }
        }
        System.out.println(Arrays.toString(arr));
    }
}